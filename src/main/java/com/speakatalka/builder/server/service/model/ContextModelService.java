package com.speakatalka.builder.server.service.model;

import com.speakatalka.builder.server.entity.context.ContextEntity;
import com.speakatalka.builder.server.model.Context;
import com.speakatalka.builder.server.service.ContextService;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
@RequiredArgsConstructor
public class ContextModelService {

    private final ContextService contextService;
    private final ContextValueModelService contextValueModelService;
    private final ExampleModelService exampleModelService;

    public List<Context> getAllContextsByStory(Integer story) {
        return contextService.getAllByVersion(story).stream()
                .map(t -> Context.builder()
                        .id(t.getContextId())
                        .values(contextValueModelService.getAllByStoryAndContext(story, t.getContextId()))
                        .to(t.getTo())
                        .x(t.getX())
                        .y(t.getY())
                        .unitId(t.getUnitId())
                        .examples(exampleModelService.getAllByContextIdAndVersionId(t.getContextId(), t.getVersionId()))
                        .build())
                .collect(Collectors.toList());
    }

    public void saveContexts(List<Context> contexts, Integer versionId) {
        contextService.saveAll(contexts.stream()
                .map(context ->
                        ContextEntity.builder()
                                .contextId(context.getId())
                                .unitId(context.getUnitId())
                                .versionId(versionId)
                                .to(context.getTo())
                                .x(context.getX())
                                .y(context.getY())
                                .build())
                .collect(Collectors.toList()));
    }
}
