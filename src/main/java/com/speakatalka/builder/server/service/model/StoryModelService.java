package com.speakatalka.builder.server.service.model;

import com.speakatalka.builder.server.model.Story;
import com.speakatalka.builder.server.service.StoryService;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import javax.transaction.Transactional;

@Singleton
@RequiredArgsConstructor
public class StoryModelService {

    private final UnitModelService unitModelService;
    private final ContextModelService contextModelService;
    private final ExampleModelService exampleModelService;
    private final ContextValueModelService contextValueModelService;
    private final StoryService storyService;

    public Story getStoryById(Integer story) {
        return Story.builder()
                .units(unitModelService.getUnitsByStoryId(story))
                .contexts(contextModelService.getAllContextsByStory(story))
                .build();
    }

    @Transactional
    public Story editStory(Story story, Integer versionId) {

        storyService.updateStoryDateChange(versionId);
        var storyUnits = story.getUnits();
        var contexts = story.getContexts();
        unitModelService.deleteAllUnitsAndSaveNew(storyUnits, versionId);
        contextModelService.saveContexts(contexts, versionId);
        exampleModelService.saveExamples(contexts, versionId);
        contextValueModelService.saveContextValues(contexts, versionId);
        return getStoryById(versionId);
    }

}
