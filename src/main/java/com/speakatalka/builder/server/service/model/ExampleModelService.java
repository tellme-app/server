package com.speakatalka.builder.server.service.model;

import com.speakatalka.builder.server.entity.example.ExampleEntity;
import com.speakatalka.builder.server.model.Context;
import com.speakatalka.builder.server.model.Example;
import com.speakatalka.builder.server.service.ExampleService;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
@RequiredArgsConstructor
public class ExampleModelService {

    private final ExampleService exampleService;

    public List<Example> getAllByContextIdAndVersionId(Integer contextId, Integer versionId) {
        return exampleService.getAllByContextIdAndVersionId(contextId, versionId)
                .stream()
                .map(t -> Example.builder()
                        .id(t.getExampleId())
                        .text(t.getExampleText())
                        .isConcrete(t.getIsConcrete())
                        .build())
                .collect(Collectors.toList());
    }

    public void saveExamples(List<Context> contexts, Integer versionId) {
        List<ExampleEntity> examples = new ArrayList<>();
        contexts.stream().filter(context -> context.getExamples() != null)
                .forEach(context -> examples.addAll(context.getExamples().stream()
                        .map(example -> ExampleEntity.builder().contextId(context.getId())
                                .exampleId(example.getId())
                                .exampleText(example.getText())
                                .isConcrete(example.getIsConcrete())
                                .versionId(versionId)
                                .build()
                        ).collect(Collectors.toList())
                ));
        exampleService.save(examples);
    }
}
