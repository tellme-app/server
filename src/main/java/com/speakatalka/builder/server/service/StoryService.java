package com.speakatalka.builder.server.service;

import com.speakatalka.builder.server.entity.StoryEntity;
import com.speakatalka.builder.server.repository.StoryRepository;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import lombok.Builder;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.util.Date;
import java.util.List;

@Singleton
@RequiredArgsConstructor
@Builder
public class StoryService {

    private final StoryRepository storyRepository;

    public Iterable<StoryEntity> getAllStories(Pageable pageable) {
        return storyRepository.findAll(pageable);
    }

    public StoryEntity saveStory(StoryEntity storyEntity) {

        if (storyEntity.getVersionId() != null)
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле versionId не должно быть заполнено");
        validateStory(storyEntity);
        storyEntity.setDateChange(new Date());
        storyEntity.setDateAdd(new Date());
        return storyRepository.save(storyEntity);
    }

    public StoryEntity editStory(StoryEntity storyEntity){
        if (storyEntity.getVersionId() != null)
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле versionId не должно быть заполнено");
        validateStory(storyEntity);
        storyEntity.setDateChange(new Date());
        return storyRepository.update(storyEntity);
    }

    public void updateStoryDateChange(Integer versionId) {
        var story = storyRepository.findById(versionId).orElseThrow(() -> new HttpStatusException(HttpStatus.BAD_REQUEST, "Истории с переданным versionId не существует"));
        story.setDateChange(new Date());
        storyRepository.update(story);
    }

    public StoryEntity getStory(Integer id){
        return storyRepository.findById(id).orElseThrow(() -> new HttpStatusException(HttpStatus.NOT_FOUND, "Истории с versionId не существует"));
    }

    public void deleteStory(Integer id) {
        if (!storyRepository.existsById(id))
            throw new HttpStatusException(HttpStatus.NOT_FOUND, "Истории с versionId не существует");
        storyRepository.deleteById(id);
    }

    private void validateStory(StoryEntity storyEntity) {
        if (storyEntity.getStoryId() == null)
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле storyId должно быть заполнено");
        if (storyEntity.getStoryName() == null || storyEntity.getStoryName().isBlank())
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле storyName должно быть заполнено");
        if (storyEntity.getIsActive() == null)
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле isActive должно быть заполнено");
        if (storyRepository.isAnotherStoryVersionActive(storyEntity.getStoryId()))
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Уже существует активная версия этой истории");
    }

    //todo переделать на record
    public List<StoryEntity> getStoryVersions(Integer storyId) {
        return storyRepository.findAllByStoryId(storyId);
    }
}
