package com.speakatalka.builder.server.service.model;

import com.speakatalka.builder.server.entity.unit.UnitEntity;
import com.speakatalka.builder.server.model.Unit;
import com.speakatalka.builder.server.service.UnitService;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Singleton
@RequiredArgsConstructor
public class UnitModelService {

    private final UnitService unitService;


    public List<Unit> getUnitsByStoryId(Integer versionId) {
        return unitService.getAllUnitsByVersionId(versionId).stream()
                .map(getUnitEntityUnitFunction())
                .collect(Collectors.toList());
    }

    public void deleteAllUnitsAndSaveNew(List<Unit> units, Integer versionId) {
        unitService.deleteAllByStoryId(versionId);
        List<UnitEntity> unitEntities = units.stream()
                .map(t -> UnitEntity.builder()
                        .unitId(t.getId())
                        .versionId(versionId)
                        .unitText(t.getText())
                        .to(t.getTo())
                        .x(t.getX())
                        .y(t.getY()).build())
                .collect(Collectors.toList());
        unitService.edit(unitEntities);
    }

    private Function<UnitEntity, Unit> getUnitEntityUnitFunction() {
        return t -> Unit.builder()
                .id(t.getUnitId())
                .text(t.getUnitText())
                .to(t.getTo())
                .x(t.getX())
                .y(t.getY())
                .build();
    }

}
