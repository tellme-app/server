package com.speakatalka.builder.server.service;

import com.speakatalka.builder.server.entity.context.ContextEntity;
import com.speakatalka.builder.server.entity.context.ContextId;
import com.speakatalka.builder.server.repository.ContextRepository;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import lombok.Builder;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.util.List;

@Singleton
@RequiredArgsConstructor
@Builder
public class ContextService {

    private final ContextRepository contextRepository;

    public Iterable<ContextEntity> getAllContexts() {
        return contextRepository.findAll();
    }

    public List<ContextEntity> getAllByVersion(Integer version) {
        return contextRepository.getAllContextByVersion(version);
    }

    public ContextEntity getContextById(ContextId id) {
        return contextRepository.findById(id).orElseThrow(() -> new HttpStatusException(HttpStatus.NOT_FOUND, "Контекст не найден"));
    }

    public ContextEntity saveContext(ContextEntity contextEntity) {
        validate(contextEntity);

        return contextRepository.save(contextEntity);
    }

    public ContextEntity updateContext(ContextEntity contextEntity) {
        validate(contextEntity);

        var contextFromDb = contextRepository.findById(contextEntity.getId()).orElseThrow(() -> new HttpStatusException(HttpStatus.BAD_REQUEST, "Контекст с заданным Id не существует"));
        if (contextFromDb.equals(contextEntity))
            return contextEntity;

        return contextRepository.save(contextEntity);
    }

    private void validate(ContextEntity contextEntity) {
        if (contextEntity.getContextId() == null)
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле contextId должно быть заполнено");
        if (contextEntity.getUnitId() == null)
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле unitId должно быть заполнено");
        if (contextEntity.getVersionId() == null)
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле versionId должно быть заполнено");
        if (contextEntity.getX() == null)
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле x должно быть заполнено");
        if (contextEntity.getY() == null)
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле y должно быть заполнено");
    }

    public void deleteContextById(ContextId id) {
        if (!contextRepository.existsById(id))
            throw new HttpStatusException(HttpStatus.NOT_FOUND, "Контекст с переданным id не найден");
        contextRepository.deleteById(id);
    }

    public boolean isContextExists(ContextId contextId) {
        return contextRepository.findById(contextId).isPresent();
    }

    public void saveAll(List<ContextEntity> contextEntities) {
        contextEntities.forEach(this::saveContext);
    }
}
