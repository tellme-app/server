package com.speakatalka.builder.server.service;

import com.speakatalka.builder.server.entity.context_values.ContextValueEntity;
import com.speakatalka.builder.server.repository.ContextValueRepository;
import lombok.AllArgsConstructor;

import javax.inject.Singleton;
import java.util.List;

@Singleton
@AllArgsConstructor
public class ContextValueService {

    private final ContextValueRepository contextValueRepository;

    public List<ContextValueEntity> getAllByVersionAndContext(Integer versionId, Integer context) {
        return contextValueRepository.findAllByVersionIdAndContextId(versionId, context);
    }

    public void saveAll(List<ContextValueEntity> contextValueEntities) {
        contextValueRepository.saveAll(contextValueEntities);
    }

    public void deleteAllByVersionId(Integer versionId) {
        contextValueRepository.deleteAllByVersionId(versionId);
    }
}
