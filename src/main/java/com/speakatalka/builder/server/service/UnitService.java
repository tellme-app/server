package com.speakatalka.builder.server.service;

import com.speakatalka.builder.server.entity.unit.UnitEntity;
import com.speakatalka.builder.server.repository.StoryRepository;
import com.speakatalka.builder.server.repository.UnitRepository;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@Singleton
@Builder
public class UnitService {

    private final UnitRepository unitRepository;
    private final StoryRepository storyRepository;

    public Iterable<UnitEntity> getAllUnits() {
        return unitRepository.findAll();
    }

    public Optional<UnitEntity> getByUnitIdAndStory(Integer id, Integer versionId) {
        return unitRepository.getByUnitIdAndVersionId(id, versionId);
    }

    public UnitEntity create(UnitEntity unitEntity) {
        validate(unitEntity);
        if (unitRepository.existsByUnitIdAndVersionId(unitEntity.getUnitId(), unitEntity.getVersionId())) {
            log.error("Блок с unitId = {} и versionId = {} уже существует", unitEntity.getUnitId(), unitEntity.getVersionId());
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, String.format("Блок с unitId=%d и versionId=%d уже существует", unitEntity.getUnitId(), unitEntity.getVersionId()));
        }
        return unitRepository.save(unitEntity);
    }

    public UnitEntity edit(UnitEntity unitEntity) {
        validate(unitEntity);

        var entityFromDb = unitRepository.getByUnitIdAndVersionId(unitEntity.getUnitId(), unitEntity.getVersionId()).orElseThrow(() -> {
            var errorString = String.format("Блок с unitId=%d и versionId=%d не существует", unitEntity.getUnitId(), unitEntity.getVersionId());
            log.error(errorString);
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, errorString);
        });
        if (entityFromDb.equals(unitEntity))
            return unitEntity;
        return unitRepository.save(unitEntity);
    }

    public void deleteById(Integer versionId, Integer unitId) {

        if (!unitRepository.existsByUnitIdAndVersionId(unitId, versionId))
            throw new HttpStatusException(HttpStatus.NOT_FOUND, "Блока с переданным id не существует");
        unitRepository.deleteByUnitIdAndVersionId(unitId, versionId);
    }

    public List<UnitEntity> getAllUnitsByVersionId(Integer versionId) {
        return unitRepository.findAllByVersionId(versionId);
    }

    private void validate(UnitEntity unitEntity) {
        if (unitEntity.getUnitId() == null)
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле id должно быть заполнено");
        if (unitEntity.getUnitText() == null || unitEntity.getUnitText().isBlank())
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле unitText должно быть заполнено");
        if (unitEntity.getX() == null)
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле 'x' должно быть заполнено");
        if (unitEntity.getY() == null)
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле 'y' должно быть заполнено");
        if (unitEntity.getVersionId() == null)
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле versionId должно быть заполнено");
        if (!storyRepository.existsById(unitEntity.getVersionId()))
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Истории с versionId=" + unitEntity.getVersionId() + " не существует");
    }

    public void edit(List<UnitEntity> units) {
        units.stream().sorted(Comparator.comparing(UnitEntity::getUnitId)).forEachOrdered(this::create);
    }

    public void deleteAllByStoryId(Integer storyId) {
        unitRepository.deleteAllUnitsByVersionId(storyId);
    }
}
