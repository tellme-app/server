package com.speakatalka.builder.server.service.model;

import com.speakatalka.builder.server.entity.context_values.ContextValueEntity;
import com.speakatalka.builder.server.model.Context;
import com.speakatalka.builder.server.model.ContextValue;
import com.speakatalka.builder.server.service.ContextValueService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
@RequiredArgsConstructor
public class ContextValueModelService {

    private final ContextValueService contextValueService;

    public List<ContextValue> getAllByStoryAndContext(Integer versionId, Integer context) {
        return contextValueService.getAllByVersionAndContext(versionId, context).stream()
                .map(t -> ContextValue.builder()
                        .id(t.getContextValuesId())
                        .value(t.getValue())
                        .build())
                .collect(Collectors.toList());
    }

    public void saveContextValues(List<Context> contexts, Integer versionId) {
        contextValueService.deleteAllByVersionId(versionId);
        List<ContextValueEntity> contextValueEntities = new ArrayList<>();
        contexts
                .forEach(context -> contextValueEntities.addAll(context.getValues().stream()
                        .map(value -> ContextValueEntity.builder()
                                .contextId(context.getId())
                                .contextValuesId(value.getId())
                                .versionId(versionId)
                                .value(value.getValue())
                                .build())
                        .collect(Collectors.toList()))
                );

        contextValueService.saveAll(contextValueEntities);
    }
}
