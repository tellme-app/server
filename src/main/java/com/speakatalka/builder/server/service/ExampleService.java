package com.speakatalka.builder.server.service;

import com.speakatalka.builder.server.entity.context.ContextId;
import com.speakatalka.builder.server.entity.example.ExampleEntity;
import com.speakatalka.builder.server.entity.example.ExampleId;
import com.speakatalka.builder.server.repository.ExampleRepository;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import lombok.Builder;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.util.List;

@Singleton
@RequiredArgsConstructor
@Builder
public class ExampleService {

    private final ExampleRepository exampleRepository;
    private final ContextService contextService;

    public Iterable<ExampleEntity> getAll() {
        return exampleRepository.findAll();
    }


    public List<ExampleEntity> getAllByContextIdAndVersionId(Integer contextId, Integer versionId) {
        return exampleRepository.findAllByContextIdAndVersionId(contextId, versionId);
    }

    public ExampleEntity getById(Integer versionId, Integer context, Integer id) {
        return exampleRepository.findById(buildId(versionId, context, id)).orElseThrow(() -> new HttpStatusException(HttpStatus.NOT_FOUND, "Пример не найден"));
    }

    public ExampleEntity save(ExampleEntity exampleEntity) {
        validate(exampleEntity);
        return exampleRepository.save(exampleEntity);
    }

    public ExampleEntity edit(ExampleEntity exampleEntity) {

        validate(exampleEntity);
        var exampleId = buildId(exampleEntity);
        var exampleFromDb = exampleRepository.findById(exampleId).orElseThrow(() -> new HttpStatusException(HttpStatus.BAD_REQUEST, "Примера с переданным exampleId не существует"));
        if (exampleFromDb.equals(exampleEntity))
            return exampleEntity;
        return exampleRepository.save(exampleEntity);
    }

    public void deleteById(Integer versionId, Integer contextId, Integer id) {
        var exampleId = buildId(versionId, contextId, id);
        validateExisting(exampleId);
        exampleRepository.deleteById(exampleId);
    }

    private void validateExisting(ExampleId id) {
        if (!exampleRepository.existsById(id))
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Примера с переданным exampleId не существует");
    }

    private void validate(ExampleEntity exampleEntity) {
        var contextId = ContextId.builder().contextId(exampleEntity.getContextId()).versionId(exampleEntity.getVersionId()).build();
        if (exampleEntity.getExampleId() == null )
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле exampleId должно быть заполнено");
        if (!contextService.isContextExists(contextId))
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Контекста с переданным contextId не существует");
        if (exampleEntity.getIsConcrete() == null)
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле isConcrete должно быть заполнено");
        if (exampleEntity.getExampleText() == null || exampleEntity.getExampleText().isBlank())
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Поле exampleText должно быть заполнено");
    }

    public void save(List<ExampleEntity> examples) {
        examples.forEach(this::save);
    }

    private ExampleId buildId(ExampleEntity exampleEntity) {
        return buildId(exampleEntity.getVersionId(), exampleEntity.getContextId(), exampleEntity.getExampleId());
    }

    private ExampleId buildId(Integer versionId, Integer contextId, Integer id) {
        return ExampleId.builder().contextId(contextId).versionId(versionId).exampleId(id).build();
    }
}
