package com.speakatalka.builder.server.model;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Introspected
public class ContextValue {

    @NotNull
    private Integer id;
    @NotNull
    @NotBlank
    private String value;
}
