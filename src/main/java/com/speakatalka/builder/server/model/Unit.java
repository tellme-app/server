package com.speakatalka.builder.server.model;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Introspected
public class Unit {

    @NotNull
    private Integer id;
    @NotNull @NotBlank
    private String text;
    @NotNull
    private Integer x;
    @NotNull
    private Integer y;
    private Integer to;
}
