package com.speakatalka.builder.server.model;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Introspected
public class Context {

    @NotNull
    private Integer id;
    @NotNull
    private Integer unitId;
    @Valid
    private List<Example> examples;
    private Integer to;
    @Valid
    private List<ContextValue> values;
    @NotNull
    private Integer x;
    @NotNull
    private Integer y;
}
