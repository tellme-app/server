package com.speakatalka.builder.server.model;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Introspected
public class Story {

    @Valid
    private List<Unit> units;
    @Valid
    private List<Context> contexts;

    public void validateUnits() {
        if (this.units != null && this.units.size() > 0 && contexts != null && contexts.size() > 0) {
            units.forEach(t -> {
                var contextPerUnit = contexts.stream().filter(context -> context.getUnitId().equals(t.getId())).count();
                if (contextPerUnit > 0 && t.getTo() != null)
                    throw new HttpStatusException(HttpStatus.BAD_REQUEST, "У блока не может быть одновременно Контекст и ссылка на другой блок");
                if (t.getTo() != null)
                    checkUnitExist(t.getTo());
            });
        }
    }

    private void checkUnitExist(Integer unitIdToFind) {
        var errorMessage = String.format("Юнита с id = %d не существует", unitIdToFind);
        if (this.units != null && this.units.size() > 0)
            this.units.stream().filter(t -> t.getId().equals(unitIdToFind)).findAny().orElseThrow(() -> new HttpStatusException(HttpStatus.BAD_REQUEST, errorMessage));
    }
}
