package com.speakatalka.builder.server.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(schema = "main", name = "story")
@Entity
public class StoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "version_id")
    private Integer versionId;

    @Column(name = "story_name")
    private String storyName;

    @Column(name = "story_desc")
    private String description;

    @Column(name = "date_add")
    private Date dateAdd;

    @Column(name = "date_change")
    private Date dateChange;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "story_id")
    private Integer storyId;
}
