package com.speakatalka.builder.server.entity.context;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ContextId implements Serializable {

    @Id
    private Integer contextId;

    @Id
    private Integer versionId;
}
