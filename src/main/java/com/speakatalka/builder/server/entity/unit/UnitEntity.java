package com.speakatalka.builder.server.entity.unit;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(schema = "main", name = "unit")
@Entity
@IdClass(UnitId.class)
public class UnitEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "unit_id")
    private Integer unitId;

    @Column(name = "unit_text")
    private String unitText;

    @Id
    @Column(name = "version_id")
    private Integer versionId;

    @Column(name = "x_axis")
    private Integer x;

    @Column(name = "y_axis")
    private Integer y;

    @Column(name = "unit_to")
    private Integer to;
}
