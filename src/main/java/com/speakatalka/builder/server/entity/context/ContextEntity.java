package com.speakatalka.builder.server.entity.context;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(schema = "main", name = "context")
@Entity
@IdClass(ContextId.class)
public class ContextEntity {

    @Id
    @Column(name = "context_id")
    private Integer contextId;

    @Column(name = "unit_id")
    private Integer unitId;

    @Id
    @Column(name = "version_id")
    private Integer versionId;

    @Column(name = "unit_to")
    private Integer to;

    @Column(name = "x_axis")
    private Integer x;

    @Column(name = "y_axis")
    private Integer y;

    @JsonIgnore
    public ContextId getId() {
        return ContextId.builder().contextId(contextId).versionId(versionId).build();
    }
}
