package com.speakatalka.builder.server.entity.example;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(schema = "main", name = "example")
@Entity
@IdClass(ExampleId.class)
public class ExampleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "example_id")
    private Integer exampleId;

    @Column(name = "example_text")
    private String exampleText;

    @Id
    @Column(name = "context_id")
    private Integer contextId;

    @Id
    @Column(name = "version_id")
    private Integer versionId;

    @Column(name = "example_is_concrete")
    private Boolean isConcrete;

    @JsonIgnore
    public ExampleId getId(){
        return ExampleId.builder().exampleId(exampleId).versionId(versionId).contextId(contextId).build();
    }
}
