package com.speakatalka.builder.server.entity.example;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExampleId implements Serializable {

    @Id
    private Integer exampleId;

    @Id
    private Integer contextId;

    @Id
    private Integer versionId;
}
