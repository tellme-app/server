package com.speakatalka.builder.server.entity.context_values;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(schema = "main", name = "context_values")
@Entity
@IdClass(ContextValueId.class)
public class ContextValueEntity {

    @Id
    @Column(name = "context_values_id")
    private Integer contextValuesId;

    @Id
    @Column(name = "context_id")
    private Integer contextId;

    @Id
    @Column(name = "version_id")
    private Integer versionId;

    @Column(name = "context_value")
    private String value;
}
