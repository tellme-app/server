package com.speakatalka.builder.server.repository;

import com.speakatalka.builder.server.entity.example.ExampleEntity;
import com.speakatalka.builder.server.entity.example.ExampleId;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

import java.util.List;

@Repository
public interface ExampleRepository extends CrudRepository<ExampleEntity, ExampleId> {

    @Query("select t from ExampleEntity t where t.contextId=:ctxId and t.versionId=:versionId")
    List<ExampleEntity> findAllByContextIdAndVersionId(Integer ctxId, Integer versionId);
}
