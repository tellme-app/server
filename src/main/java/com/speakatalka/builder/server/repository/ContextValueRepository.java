package com.speakatalka.builder.server.repository;

import com.speakatalka.builder.server.entity.context_values.ContextValueEntity;
import com.speakatalka.builder.server.entity.context_values.ContextValueId;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

import java.util.List;

@Repository
public interface ContextValueRepository extends CrudRepository<ContextValueEntity, ContextValueId> {
    @Query("select t from ContextValueEntity t where t.versionId=:versionId and t.contextId=:contextId")
    List<ContextValueEntity> findAllByVersionIdAndContextId(Integer versionId, Integer contextId);

    @Query("delete from ContextValueEntity t where t.versionId=:versionId")
    void deleteAllByVersionId(Integer versionId);
}
