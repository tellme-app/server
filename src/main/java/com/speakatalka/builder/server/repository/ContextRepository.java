package com.speakatalka.builder.server.repository;

import com.speakatalka.builder.server.entity.context.ContextEntity;
import com.speakatalka.builder.server.entity.context.ContextId;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

import java.util.List;

@Repository
public interface ContextRepository extends CrudRepository<ContextEntity, ContextId> {

    @Query("select t from ContextEntity t where t.versionId=:versionId ")
    List<ContextEntity> getAllContextByVersion(Integer versionId);
}
