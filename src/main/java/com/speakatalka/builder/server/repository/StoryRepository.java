package com.speakatalka.builder.server.repository;

import com.speakatalka.builder.server.entity.StoryEntity;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

import java.util.List;

@Repository
public interface StoryRepository extends PageableRepository<StoryEntity, Integer> {
    Boolean existsByStoryName(String storyName);
    List<StoryEntity> findAllByStoryId(Integer storyId);

    @Query("select case when count(t) > 0 then true else false end from StoryEntity t where t.storyId=:storyId and t.isActive=true")
    Boolean isAnotherStoryVersionActive(Integer storyId);
}
