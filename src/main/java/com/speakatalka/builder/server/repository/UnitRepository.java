package com.speakatalka.builder.server.repository;

import com.speakatalka.builder.server.entity.unit.UnitEntity;
import com.speakatalka.builder.server.entity.unit.UnitId;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UnitRepository extends CrudRepository<UnitEntity, UnitId> {

    @Query("select t from UnitEntity t where t.versionId=:versionId")
    List<UnitEntity> findAllByVersionId(Integer versionId);

    @Query("select case when count(t) > 0 then true else false end from UnitEntity t where t.unitId=:unitId and t.versionId=:versionId")
    Boolean existsByUnitIdAndVersionId(Integer unitId, Integer versionId);

    @Query("select t from UnitEntity t where t.unitId=:unitId and t.versionId=:versionId")
    Optional<UnitEntity> getByUnitIdAndVersionId(Integer unitId, Integer versionId);

    @Query("delete from UnitEntity t where t.versionId=:versionId")
    void deleteAllUnitsByVersionId(Integer versionId);

    @Query("delete from UnitEntity t where t.unitId=:unitId and t.versionId=:versionId")
    void deleteByUnitIdAndVersionId(Integer unitId, Integer versionId);
}
