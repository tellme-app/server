package com.speakatalka.builder.server.controller;

import com.speakatalka.builder.server.entity.context.ContextEntity;
import com.speakatalka.builder.server.entity.context.ContextId;
import com.speakatalka.builder.server.service.ContextService;
import io.micronaut.http.annotation.*;
import lombok.RequiredArgsConstructor;

@Controller("/context")
@RequiredArgsConstructor
public class ContextController {

    private final ContextService contextService;

    @Get
    public Iterable<ContextEntity> getAllContexts() {
        return contextService.getAllContexts();
    }

    @Get("/{versionId}/{id}")
    public ContextEntity getContextById(@PathVariable Integer versionId, @PathVariable Integer id) {
        return contextService.getContextById(getId(versionId, id));
    }

    @Post
    public ContextEntity createContext(@Body ContextEntity contextEntity) {
        return contextService.saveContext(contextEntity);
    }

    @Put
    public ContextEntity updateContext(@Body ContextEntity contextEntity) {
        return contextService.updateContext(contextEntity);
    }

    @Delete("/{versionId}/{id}")
    public void deleteContextById(@PathVariable Integer versionId, @PathVariable Integer id){
        contextService.deleteContextById(getId(versionId, id));
    }

    private ContextId getId(Integer versionId, Integer id){
        return ContextId.builder().versionId(versionId).contextId(id).build();
    }
}
