package com.speakatalka.builder.server.controller;

import com.speakatalka.builder.server.entity.example.ExampleEntity;
import com.speakatalka.builder.server.service.ExampleService;
import io.micronaut.http.annotation.*;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Controller("/example")
public class ExampleController {

    private final ExampleService exampleService;

    @Get
    public Iterable<ExampleEntity> getAll() {
        return exampleService.getAll();
    }

    @Get("/{versionId}/{context}/{id}")
    public ExampleEntity getById(@PathVariable Integer versionId, @PathVariable Integer context, @PathVariable Integer id) {
        return exampleService.getById(versionId, context, id);
    }

    @Post
    public ExampleEntity save(@Body ExampleEntity exampleEntity) {
        return exampleService.save(exampleEntity);
    }

    @Put
    public ExampleEntity edit(@Body ExampleEntity exampleEntity) {
        return exampleService.edit(exampleEntity);
    }

    @Delete("/{story}/{context}/{id}")
    public void deleteById(@PathVariable Integer story, @PathVariable Integer context, @PathVariable Integer id) {
        exampleService.deleteById(story, context, id);
    }


}
