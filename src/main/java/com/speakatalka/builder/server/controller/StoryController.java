package com.speakatalka.builder.server.controller;

import com.speakatalka.builder.server.entity.StoryEntity;
import com.speakatalka.builder.server.service.StoryService;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Controller("/story")
@RequiredArgsConstructor
@Tag(name = "app")
@Secured(SecurityRule.IS_AUTHENTICATED)
public class StoryController {

    private final StoryService storyService;

    @Get("/version/{storyId}")
    public List<StoryEntity> getStoryVersions(@PathVariable Integer storyId) {
        return storyService.getStoryVersions(storyId);
    }

    @Get
    public Iterable<StoryEntity> getAllStories(Pageable pageable) {
        return storyService.getAllStories(pageable);
    }

    @Get("/{versionId}")
    public StoryEntity getStoryById(@PathVariable Integer versionId){
        return storyService.getStory(versionId);
    }

    @Post
    public StoryEntity saveStory(@Body StoryEntity storyEntity){
        return storyService.saveStory(storyEntity);
    }

    @Put
    public StoryEntity editStory(@Body StoryEntity storyEntity){
        return storyService.editStory(storyEntity);
    }

    @Delete("/{versionId}")
    public void deleteStory(@PathVariable Integer versionId){
        storyService.deleteStory(versionId);
    }
}
