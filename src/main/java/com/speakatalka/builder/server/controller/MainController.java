package com.speakatalka.builder.server.controller;

import com.speakatalka.builder.server.model.Story;
import com.speakatalka.builder.server.service.model.StoryModelService;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

import javax.validation.Valid;

@Controller("/story/{versionId}/units")
@RequiredArgsConstructor
@Validated
@Tag(name = "app")
@Secured(SecurityRule.IS_AUTHENTICATED)
public class MainController {

    private final StoryModelService storyModelService;

    @Get
    public Story getUnitsByStoryId(@PathVariable Integer versionId){
        return storyModelService.getStoryById(versionId);
    }

    @Put
    public Story editStory(@Body @Valid Story story, @PathVariable Integer versionId) {
        return storyModelService.editStory(story, versionId);
    }
}
