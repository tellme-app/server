package com.speakatalka.builder.server.controller;

import com.speakatalka.builder.server.entity.unit.UnitEntity;
import com.speakatalka.builder.server.entity.unit.UnitId;
import com.speakatalka.builder.server.service.UnitService;
import io.micronaut.http.annotation.*;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
@Controller("/unit")
public class UnitController {

    private final UnitService unitService;

    @Get
    public Iterable<UnitEntity> getAllUnits() {
        return unitService.getAllUnits();
    }

    @Get("/{versionId}/{id}")
    public Optional<UnitEntity> getById(@PathVariable Integer versionId, @PathVariable Integer id) {
        return unitService.getByUnitIdAndStory(id, versionId);
    }

    @Post
    public UnitEntity saveUnit(@Body UnitEntity unitEntity) {
        return unitService.create(unitEntity);
    }

    @Put
    public UnitEntity edit(@Body UnitEntity unitEntity) {
        return unitService.edit(unitEntity);
    }

    @Delete("/{versionId}/{unitId}")
    public void deleteById(@PathVariable Integer versionId, @PathVariable Integer unitId) {
        unitService.deleteById(versionId, unitId);
    }
}
