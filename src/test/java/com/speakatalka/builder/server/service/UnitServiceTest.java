//package com.speakatalka.builder.server.service;
//
//import com.speakatalka.builder.server.entity.unit.UnitEntity;
//import com.speakatalka.builder.server.repository.StoryRepository;
//import com.speakatalka.builder.server.repository.UnitRepository;
//import io.micronaut.http.HttpStatus;
//import io.micronaut.http.exceptions.HttpStatusException;
//import io.micronaut.test.annotation.MicronautTest;
//import io.micronaut.test.annotation.MockBean;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import javax.inject.Inject;
//
//import java.util.Optional;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertThrows;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.when;
//
//@MicronautTest
//class UnitServiceTest {
//
//    @Inject
//    UnitRepository unitRepository;
//    @Inject
//    StoryRepository storyRepository;
//
//    UnitService unitService;
//
//    @BeforeEach
//    void setUp() {
//        when(storyRepository.existsById(1)).then(inv -> true);
//        when(storyRepository.existsById(2)).then(inv -> false);
//        when(unitRepository.existsByUnitIdAndVersionId(1, 1)).then(inv -> false);
//        when(unitRepository.existsByUnitIdAndVersionId(2, 1)).then(inv -> true);
//        when(unitRepository.getByUnitIdAndVersionId(1, 1)).then(inv -> Optional.of(UnitEntity.builder().unitText("success").x(0).y(0).story(1).unitId(1).build()));
//        this.unitService = UnitService.builder().unitRepository(unitRepository).storyRepository(storyRepository).build();
//    }
//
//    @Test
//    void create_success() {
//        var unit = UnitEntity.builder().unitId(1).unitText("success").story(1).x(0).y(0).build();
//        unitService.create(unit);
//    }
//
//    @Test
//    void edit_success() {
//        var unit = UnitEntity.builder().unitText("success").x(0).y(0).story(1).unitId(1).build();
//        unitService.edit(unit);
//    }
//
//    @Test
//    void create_notSuccess_unitByUnitIdAndStoryIdAlreadyExists() {
//        var unit = UnitEntity.builder().unitId(2).story(1).unitText("notSuccess").x(0).y(0).build();
//        var ex = assertThrows(HttpStatusException.class, () -> unitService.create(unit));
//        assertEquals(HttpStatus.BAD_REQUEST, ex.getStatus());
//        assertEquals(String.format("Блок с unitId=%d и story=%d уже существует", unit.getUnitId(), unit.getStory()), ex.getMessage());
//    }
//
//    @Test
//    void create_edit_notSuccess_unitIdIsNotSet() {
//        var unit = UnitEntity.builder().story(1).x(0).y(0).unitText("notSuccess").build();
//        var exCreate = assertThrows(HttpStatusException.class, () -> unitService.create(unit));
//        var exEdit = assertThrows(HttpStatusException.class, () -> unitService.edit(unit));
//        assertEquals(HttpStatus.BAD_REQUEST, exCreate.getStatus());
//        assertEquals(HttpStatus.BAD_REQUEST, exEdit.getStatus());
//        assertEquals("Поле id должно быть заполнено", exCreate.getMessage());
//        assertEquals("Поле id должно быть заполнено", exEdit.getMessage());
//    }
//
//    @Test
//    void create_edit_notSuccess_unitTextIsNull() {
//        var unit = UnitEntity.builder().unitId(1).story(1).x(0).y(0).build();
//        var exCreate = assertThrows(HttpStatusException.class, () -> unitService.create(unit));
//        var exEdit = assertThrows(HttpStatusException.class, () -> unitService.edit(unit));
//        assertEquals(HttpStatus.BAD_REQUEST, exCreate.getStatus());
//        assertEquals(HttpStatus.BAD_REQUEST, exEdit.getStatus());
//        assertEquals("Поле unitText должно быть заполнено", exCreate.getMessage());
//        assertEquals("Поле unitText должно быть заполнено", exEdit.getMessage());
//    }
//
//    @Test
//    void create_edit_notSuccess_unitTextIsBlank() {
//        var unit = UnitEntity.builder().unitId(1).story(1).unitText("").x(0).y(0).build();
//        var exCreate = assertThrows(HttpStatusException.class, () -> unitService.create(unit));
//        var exEdit = assertThrows(HttpStatusException.class, () -> unitService.edit(unit));
//        assertEquals(HttpStatus.BAD_REQUEST, exCreate.getStatus());
//        assertEquals(HttpStatus.BAD_REQUEST, exEdit.getStatus());
//        assertEquals("Поле unitText должно быть заполнено", exCreate.getMessage());
//        assertEquals("Поле unitText должно быть заполнено", exEdit.getMessage());
//    }
//
//    @Test
//    void create_edit_notSuccess_xIsNotSet() {
//        var unit = UnitEntity.builder().unitId(1).story(1).unitText("notSuccess").y(0).build();
//        var exCreate = assertThrows(HttpStatusException.class, () -> unitService.create(unit));
//        var exEdit = assertThrows(HttpStatusException.class, () -> unitService.edit(unit));
//        assertEquals(HttpStatus.BAD_REQUEST, exCreate.getStatus());
//        assertEquals(HttpStatus.BAD_REQUEST, exEdit.getStatus());
//        assertEquals("Поле 'x' должно быть заполнено", exCreate.getMessage());
//        assertEquals("Поле 'x' должно быть заполнено", exEdit.getMessage());
//    }
//
//    @Test
//    void create_edit_notSuccess_yIsNotSet() {
//        var unit = UnitEntity.builder().unitId(1).story(1).unitText("notSuccess").x(0).build();
//        var exCreate = assertThrows(HttpStatusException.class, () -> unitService.create(unit));
//        var exEdit = assertThrows(HttpStatusException.class, () -> unitService.edit(unit));
//        assertEquals(HttpStatus.BAD_REQUEST, exCreate.getStatus());
//        assertEquals(HttpStatus.BAD_REQUEST, exEdit.getStatus());
//        assertEquals("Поле 'y' должно быть заполнено", exCreate.getMessage());
//        assertEquals("Поле 'y' должно быть заполнено", exEdit.getMessage());
//    }
//
//    @Test
//    void create_edit_notSuccess_storyIdIsNotSet() {
//        var unit = UnitEntity.builder().unitId(1).y(0).x(0).unitText("notSuccess").build();
//        var exCreate = assertThrows(HttpStatusException.class, () -> unitService.create(unit));
//        var exEdit = assertThrows(HttpStatusException.class, () -> unitService.edit(unit));
//        assertEquals(HttpStatus.BAD_REQUEST, exCreate.getStatus());
//        assertEquals(HttpStatus.BAD_REQUEST, exEdit.getStatus());
//        assertEquals("Поле story не должно быть пустым", exCreate.getMessage());
//        assertEquals("Поле story не должно быть пустым", exEdit.getMessage());
//    }
//
//    @Test
//    void create_edit_notSuccess_storyIsNotExists() {
//        var unit = UnitEntity.builder().unitId(1).story(2).unitText("notSuccess").x(0).y(0).build();
//        var exCreate = assertThrows(HttpStatusException.class, () -> unitService.create(unit));
//        var exEdit = assertThrows(HttpStatusException.class, () -> unitService.edit(unit));
//        assertEquals(HttpStatus.BAD_REQUEST, exCreate.getStatus());
//        assertEquals(HttpStatus.BAD_REQUEST, exEdit.getStatus());
//        assertEquals("Истории с id=" + unit.getStory() + " не существует", exCreate.getMessage());
//        assertEquals("Истории с id=" + unit.getStory() + " не существует", exEdit.getMessage());
//    }
//
//    @Test
//    void getAllUnitsByStory() {
//    }
//
//    @MockBean(UnitRepository.class)
//    public UnitRepository unitRepository(){
//        return mock(UnitRepository.class);
//    }
//
//    @MockBean(StoryRepository.class)
//    public StoryRepository storyRepository(){
//        return mock(StoryRepository.class);
//    }
//}
