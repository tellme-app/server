package com.speakatalka.builder.server.service;

import com.speakatalka.builder.server.entity.context.ContextEntity;
import com.speakatalka.builder.server.entity.context.ContextId;
import com.speakatalka.builder.server.repository.ContextRepository;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import io.micronaut.test.annotation.MicronautTest;
import io.micronaut.test.annotation.MockBean;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@MicronautTest
class ContextServiceTest {

    @Inject
    ContextRepository contextRepository;

    ContextService contextService;
    @BeforeEach
    void setUp() {
        var existingId = ContextId.builder().contextId(1).story(1).build();
        var notExistingId = ContextId.builder().contextId(2).story(1).build();
        when(contextRepository.existsById(notExistingId)).then(inv -> false);
        when(contextRepository.existsById(existingId)).then(inv -> true);
        when(contextRepository.findById(existingId)).then(inv -> Optional.of(ContextEntity.builder().contextId(1).x(0).y(0).story(1).to(1).unitId(1).build()));
        this.contextService = ContextService.builder().contextRepository(contextRepository).build();
    }

    @Test
    void saveContext_updateContext_success() {
        var context = ContextEntity.builder().contextId(1).x(0).y(0).story(1).to(1).unitId(1).build();
        contextService.saveContext(context);
        contextService.updateContext(context);
    }

    @Test
    void updateContext_notSuccess_contextIsNotExists() {
        var context = ContextEntity.builder().contextId(2).x(0).y(0).story(1).to(1).unitId(1).build();
        var ex = assertThrows(HttpStatusException.class, () -> contextService.updateContext(context));
        assertEquals(HttpStatus.BAD_REQUEST, ex.getStatus());
        assertEquals("Контекст с заданным Id не существует", ex.getMessage());
    }

    @Test
    void saveContext_updateContext_notSuccess_contextIdIsNull() {
        var context = ContextEntity.builder().x(0).y(0).story(1).to(1).unitId(1).build();
        HttpStatusException updateContext_statusException = assertThrows(HttpStatusException.class, () -> contextService.updateContext(context));
        HttpStatusException saveContext_statusException = assertThrows(HttpStatusException.class, () -> contextService.updateContext(context));
        assertEquals(HttpStatus.BAD_REQUEST, updateContext_statusException.getStatus());
        assertEquals("Поле contextId должно быть заполнено", updateContext_statusException.getMessage());
        assertEquals(HttpStatus.BAD_REQUEST, saveContext_statusException.getStatus());
        assertEquals("Поле contextId должно быть заполнено", saveContext_statusException.getMessage());
    }

    @Test
    void saveContext_updateContext_notSuccess_unitIdIsNull() {
        var context = ContextEntity.builder().contextId(1).x(0).y(0).story(1).to(1).build();
        HttpStatusException updateContext_statusException = assertThrows(HttpStatusException.class, () -> contextService.updateContext(context));
        HttpStatusException saveContext_statusException = assertThrows(HttpStatusException.class, () -> contextService.updateContext(context));
        assertEquals(HttpStatus.BAD_REQUEST, updateContext_statusException.getStatus());
        assertEquals("Поле unitId должно быть заполнено", updateContext_statusException.getMessage());
        assertEquals(HttpStatus.BAD_REQUEST, saveContext_statusException.getStatus());
        assertEquals("Поле unitId должно быть заполнено", saveContext_statusException.getMessage());
    }

    @Test
    void saveContext_updateContext_notSuccess_storyIdIsNull() {
        var context = ContextEntity.builder().contextId(1).x(0).y(0).to(1).unitId(1).build();
        HttpStatusException updateContext_statusException = assertThrows(HttpStatusException.class, () -> contextService.updateContext(context));
        HttpStatusException saveContext_statusException = assertThrows(HttpStatusException.class, () -> contextService.updateContext(context));
        assertEquals(HttpStatus.BAD_REQUEST, updateContext_statusException.getStatus());
        assertEquals("Поле story должно быть заполнено", updateContext_statusException.getMessage());
        assertEquals(HttpStatus.BAD_REQUEST, saveContext_statusException.getStatus());
        assertEquals("Поле story должно быть заполнено", saveContext_statusException.getMessage());
    }

    @Test
    void saveContext_updateContext_notSuccess_xIsNull() {
        var context = ContextEntity.builder().contextId(1).y(0).to(1).unitId(1).story(1).build();
        HttpStatusException updateContext_statusException = assertThrows(HttpStatusException.class, () -> contextService.updateContext(context));
        HttpStatusException saveContext_statusException = assertThrows(HttpStatusException.class, () -> contextService.updateContext(context));
        assertEquals(HttpStatus.BAD_REQUEST, updateContext_statusException.getStatus());
        assertEquals("Поле x должно быть заполнено", updateContext_statusException.getMessage());
        assertEquals(HttpStatus.BAD_REQUEST, saveContext_statusException.getStatus());
        assertEquals("Поле x должно быть заполнено", saveContext_statusException.getMessage());
    }

    @Test
    void saveContext_updateContext_notSuccess_yIsNull() {
        var context = ContextEntity.builder().contextId(1).x(0).to(1).unitId(1).story(1).build();
        HttpStatusException updateContext_statusException = assertThrows(HttpStatusException.class, () -> contextService.updateContext(context));
        HttpStatusException saveContext_statusException = assertThrows(HttpStatusException.class, () -> contextService.updateContext(context));
        assertEquals(HttpStatus.BAD_REQUEST, updateContext_statusException.getStatus());
        assertEquals("Поле y должно быть заполнено", updateContext_statusException.getMessage());
        assertEquals(HttpStatus.BAD_REQUEST, saveContext_statusException.getStatus());
        assertEquals("Поле y должно быть заполнено", saveContext_statusException.getMessage());
    }

    @MockBean(ContextRepository.class)
    public ContextRepository contextRepository() {
        return mock(ContextRepository.class);
    }

}
