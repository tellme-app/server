package com.speakatalka.builder.server.service;

import com.speakatalka.builder.server.entity.context.ContextId;
import com.speakatalka.builder.server.entity.example.ExampleEntity;
import com.speakatalka.builder.server.entity.example.ExampleId;
import com.speakatalka.builder.server.repository.ContextRepository;
import com.speakatalka.builder.server.repository.ExampleRepository;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import io.micronaut.test.annotation.MicronautTest;
import io.micronaut.test.annotation.MockBean;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

import javax.inject.Inject;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
class ExampleServiceTest {

    ExampleService exampleService;

    @Inject
    ContextService contextService;
    @Inject
    ExampleRepository exampleRepository;

    @BeforeEach
    public void setUp(){
        var contextExistingId = ContextId.builder().contextId(1).story(1).build();
        var contextNotExistingId = ContextId.builder().contextId(2).story(1).build();
        var exampleExistingId = ExampleId.builder().exampleId(1).contextId(1).story(1).build();
        var exampleNotExistingId = ExampleId.builder().exampleId(2).contextId(1).story(1).build();
        when(contextService.isContextExists(contextExistingId)).then(inv -> true);
        when(contextService.isContextExists(contextNotExistingId)).then(inv -> false);
        when(exampleRepository.existsById(exampleExistingId)).then(inv -> true);
        when(exampleRepository.existsById(exampleNotExistingId)).then(inv -> false);
        when(exampleRepository.findById(exampleExistingId)).then(inv -> Optional.of(ExampleEntity.builder().exampleId(1).exampleText("success").contextId(1).isConcrete(true).story(1).build()));
        this.exampleService = ExampleService.builder().contextService(contextService).exampleRepository(exampleRepository).build();
    }

    @Test
    void edit_unsuccessful_exampleIdIsNotExist() {
        var example = ExampleEntity.builder().exampleId(2).exampleText("not success").contextId(1).story(1).isConcrete(true).build();
        HttpStatusException throwable = assertThrows(HttpStatusException.class, () -> exampleService.edit(example));
        assertEquals(HttpStatus.BAD_REQUEST, throwable.getStatus());
        assertEquals("Примера с переданным exampleId не существует", throwable.getMessage());
    }

    @Test
    void save_edit_successful() {
        var example = ExampleEntity.builder().exampleId(1).story(1).isConcrete(true).exampleText("test").contextId(1).build();
        exampleService.save(example);
        exampleService.edit(example);
    }

    @Test
    void save_edit_unsuccessful_exampleIdContextIdIsNotExist() {
        var example = ExampleEntity.builder().exampleId(1).story(1).isConcrete(true).exampleText("test").contextId(2).build();
        HttpStatusException save_exception = assertThrows(HttpStatusException.class, () -> exampleService.save(example));
        assertEquals(HttpStatus.BAD_REQUEST, save_exception.getStatus());
        assertEquals("Контекста с переданным contextId не существует", save_exception.getMessage());

        HttpStatusException edit_exception = assertThrows(HttpStatusException.class, () -> exampleService.edit(example));
        assertEquals(HttpStatus.BAD_REQUEST, edit_exception.getStatus());
        assertEquals("Контекста с переданным contextId не существует", edit_exception.getMessage());
    }

    @Test
    void save_edit_unsuccessful_exampleTextIsNull() {
        var example = ExampleEntity.builder().contextId(1).exampleId(1).story(1).isConcrete(true).build();
        HttpStatusException save_exception = assertThrows(HttpStatusException.class, () -> exampleService.save(example));
        assertEquals(HttpStatus.BAD_REQUEST, save_exception.getStatus());
        assertEquals("Поле exampleText должно быть заполнено", save_exception.getMessage());

        HttpStatusException edit_exception = assertThrows(HttpStatusException.class, () -> exampleService.edit(example));
        assertEquals(HttpStatus.BAD_REQUEST, edit_exception.getStatus());
        assertEquals("Поле exampleText должно быть заполнено", edit_exception.getMessage());
    }

    @Test
    void save_edit_unsuccessful_exampleTextIsBlank() {
        var example = ExampleEntity.builder().exampleText("").contextId(1).story(1).isConcrete(true).exampleId(1).build();
        HttpStatusException save_exception = assertThrows(HttpStatusException.class, () -> exampleService.save(example));
        assertEquals(HttpStatus.BAD_REQUEST, save_exception.getStatus());
        assertEquals("Поле exampleText должно быть заполнено", save_exception.getMessage());

        HttpStatusException edit_exception = assertThrows(HttpStatusException.class, () -> exampleService.edit(example));
        assertEquals(HttpStatus.BAD_REQUEST, edit_exception.getStatus());
        assertEquals("Поле exampleText должно быть заполнено", edit_exception.getMessage());
    }

    @Test
    void save_edit_unsuccessful_exampleIdIsNotSet() {
        var example = ExampleEntity.builder().exampleText("not success").contextId(1).story(1).isConcrete(false).build();
        HttpStatusException edit_exception = assertThrows(HttpStatusException.class, () -> exampleService.edit(example));
        assertEquals(HttpStatus.BAD_REQUEST, edit_exception.getStatus());
        assertEquals("Поле exampleId должно быть заполнено", edit_exception.getMessage());

        HttpStatusException save_exception = assertThrows(HttpStatusException.class, () -> exampleService.save(example));
        assertEquals(HttpStatus.BAD_REQUEST, save_exception.getStatus());
        assertEquals("Поле exampleId должно быть заполнено", save_exception.getMessage());
    }

    @Test
    void save_edit_unsuccessful_isConcreteIsNotSet() {
        var example = ExampleEntity.builder().exampleText("not success").contextId(1).story(1).exampleId(1).build();
        HttpStatusException edit_exception = assertThrows(HttpStatusException.class, () -> exampleService.edit(example));
        assertEquals(HttpStatus.BAD_REQUEST, edit_exception.getStatus());
        assertEquals("Поле isConcrete должно быть заполнено", edit_exception.getMessage());

        HttpStatusException save_exception = assertThrows(HttpStatusException.class, () -> exampleService.save(example));
        assertEquals(HttpStatus.BAD_REQUEST, save_exception.getStatus());
        assertEquals("Поле isConcrete должно быть заполнено", save_exception.getMessage());
    }

    @MockBean(ContextService.class)
    public ContextService contextService() {
        return mock(ContextService.class);
    }

    @MockBean(ExampleRepository.class)
    public ExampleRepository exampleRepository() {
        return mock(ExampleRepository.class);
    }
}
