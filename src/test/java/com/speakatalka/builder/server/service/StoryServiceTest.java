package com.speakatalka.builder.server.service;

import com.speakatalka.builder.server.entity.StoryEntity;
import com.speakatalka.builder.server.repository.StoryRepository;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import io.micronaut.test.annotation.MicronautTest;
import io.micronaut.test.annotation.MockBean;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@MicronautTest
class StoryServiceTest {

    StoryService storyService;
    StoryEntity storySuccess, story_notSuccess_idIsSet, story_notSuccess_storyNameIsNull, story_notSuccess_storyNameIsBlank, story_notSuccess_storyNameAlreadyExists;

    @Inject
    StoryRepository storyRepository;

    @BeforeEach
    void setUp() {
        storySuccess = StoryEntity.builder().storyName("success").build();
        story_notSuccess_idIsSet = StoryEntity.builder().storyId(1).storyName("notSuccess").build();
        story_notSuccess_storyNameIsNull = StoryEntity.builder().build();
        story_notSuccess_storyNameIsBlank = StoryEntity.builder().storyName("").build();
        story_notSuccess_storyNameAlreadyExists = StoryEntity.builder().storyName("alreadyExists").build();
        when(storyRepository.existsByStoryName("success")).then(inv -> false);
        when(storyRepository.existsByStoryName("alreadyExists")).then(inv -> true);
        this.storyService = StoryService.builder().storyRepository(storyRepository).build();
    }

    @Test
    void saveStory_success() {
        storyService.saveStory(storySuccess);
    }

    @Test
    void saveStory_notSuccess_storyIdIsSet() {
        HttpStatusException statusException = assertThrows(HttpStatusException.class, () -> storyService.saveStory(story_notSuccess_idIsSet));
        assertEquals(HttpStatus.BAD_REQUEST, statusException.getStatus());
        assertEquals("Поле id не должно быть заполнено", statusException.getMessage());
    }

    @Test
    void saveStory_notSuccess_storyNameIsNull() {
        HttpStatusException statusException = assertThrows(HttpStatusException.class, () -> storyService.saveStory(story_notSuccess_storyNameIsNull));
        assertEquals(HttpStatus.BAD_REQUEST, statusException.getStatus());
        assertEquals("Поле storyName должно быть заполнено", statusException.getMessage());
    }

    @Test
    void saveStory_notSuccess_storyNameIsBlank() {
        HttpStatusException statusException = assertThrows(HttpStatusException.class, () -> storyService.saveStory(story_notSuccess_storyNameIsBlank));
        assertEquals(HttpStatus.BAD_REQUEST, statusException.getStatus());
        assertEquals("Поле storyName должно быть заполнено", statusException.getMessage());
    }

    @Test
    void editStory_success() {
        var story = StoryEntity.builder().storyId(1).storyName("success").build();
        storyService.editStory(story);
    }

    @Test
    void editStory_notSuccess_storyIdIsNotSet(){
        var story = StoryEntity.builder().storyName("notSuccess").build();
        HttpStatusException statusException = assertThrows(HttpStatusException.class, () -> storyService.editStory(story));
        assertEquals(HttpStatus.BAD_REQUEST, statusException.getStatus());
        assertEquals("Поле id должно быть заполнено", statusException.getMessage());
    }

    @Test
    void editStory_notSuccess_storyNameIsNull() {
        var story = StoryEntity.builder().storyId(1).build();
        HttpStatusException statusException = assertThrows(HttpStatusException.class, () -> storyService.editStory(story));
        assertEquals(HttpStatus.BAD_REQUEST, statusException.getStatus());
        assertEquals("Поле storyName должно быть заполнено", statusException.getMessage());
    }

    @Test
    void editStory_notSuccess_storyNameIsBlank() {
        var story = StoryEntity.builder().storyId(1).storyName("").build();
        HttpStatusException statusException = assertThrows(HttpStatusException.class, () -> storyService.editStory(story));
        assertEquals(HttpStatus.BAD_REQUEST, statusException.getStatus());
        assertEquals("Поле storyName должно быть заполнено", statusException.getMessage());
    }

    @MockBean(StoryRepository.class)
    public StoryRepository storyRepository() {
        return mock(StoryRepository.class);
    }
}
