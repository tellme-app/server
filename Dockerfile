FROM openjdk:14-alpine as build
WORKDIR /app
COPY . /app
RUN ./gradlew build -x test

FROM oracle/graalvm-ce:20.3.0-java11 AS graalvm
RUN gu install native-image
WORKDIR /home/app
COPY --from=build /app/build/layers/libs /home/app/libs
COPY --from=build /app/build/layers/resources /home/app/resources
COPY --from=build /app/build/layers/application.jar /home/app/application.jar
RUN native-image -H:Class=com.speakatalka.builder.server.Application -H:Name=application --no-fallback -cp /home/app/libs/*.jar:/home/app/resources:/home/app/application.jar

FROM frolvlad/alpine-glibc
RUN apk update && apk add libstdc++
COPY --from=graalvm /home/app/application /app/application
ENTRYPOINT ["/app/application"]
